<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Sign up</title>
</head>
<body>
	<form method="post" action="Auth.php">
		<table>
			<tr>
				<h1>Free sign up at 10<font color="red">X</font></h1>
			</tr>
			<tr>
				<p>First name</p>
			</tr>
			<tr>
				<input type="text" name="FirstName" placeholder="First name" required>
			</tr>
			<tr>
				<p>Last name</p>
			</tr>
			<tr>
				<input type="text" name="LastName" placeholder="Last name" required>
			</tr>
			<tr>
				<p>Email</p>
			</tr>
			<tr>
				<input type="Email" name="email" placeholder="exemple@exemple.com">
			</tr>
			<tr>
				<p>Username</p>
			</tr>
			<tr>
				<input type="Username" name="UserName" placeholder="Username" required>
			</tr>
			<tr>
				<p>Password</p>
			</tr>
			<tr>
				<input type="Password" name="Password1" placeholder="Password" required>
			</tr>
			<tr>
				<p>Re-enter password</p>
			</tr>
			<tr>
				<input type="Password" name="Password2" placeholder="Re-enter password" required>
			</tr>
			<tr>
				<p></p>
			</tr>
			<tr>
				<input type="submit" name="sign up" value="Sign up">
			</tr>
			<tr>
				<p>
				<?php 
					if (isset($_GET['error'])){
						$errorMssg = $_GET['error'];
				 		if ($errorMssg != "")
				 		echo "<br>".$errorMssg;
				 		else
				 			echo "Your account is successfully created! You can login now!";
				 	}
				 ?>
				 </p>
			</tr>
		</table>
		<H2>Already Signed up? <a href="Index.php"> Log in here.</a></H2>
	</form>
</body>
</html>