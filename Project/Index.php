<?php  
	session_start();
	if(isset($_SESSION['userType'])){
		if($_SESSION['userType'] == "user")
			header('location:Home.php');
		elseif($_SESSION['userType'] == "admin")
			header('location:HomeAdmin.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>File System Server</title>
</head>
<body>
	<?php include('Login.php'); ?>
	<p>New to 10<font color="red">X</font> File System Server! <a href="Sign_up.php"> Sign up</a></p>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>