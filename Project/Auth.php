<?php 
	
	// ------------------------ functions start here --------------------------------------------------

	include ('Function.php');

	// ------------------------ functions stops here --------------------------------------------------
	
	// ------------------------ Login authentication-----------------------------------
	
	if(isset($_POST['username']) && isset($_POST['password'])){
		
		//check if password & username exist and match
		$user = $_POST['username'];
		$passwordLogin = $_POST['password'];

		// check recaptcha
		$secretKey = "6LcdSIsUAAAAAKGPLk21On6RCVhykPMkRot4qP07";
		$responseKey = $_POST["g-recaptcha-response"];
		$userIP = $_SERVER['REMOTE_ADDR'];

		$url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
		$response = file_get_contents($url);
		$response = json_decode($response);
		if($response->success){
			echo "Captcha ok";
			$username = checkUsernameLogin("Protected/AccountAccepted.txt",$user); // return a string or ""
			$checkLogin = allowUserConnect("Protected/AccountAccepted.txt",$passwordLogin,$username);
			if ($checkLogin == 1){
				session_start();
				$_SESSION['id'] = 1;
				$_SESSION['connect'] ="active";

				$_SESSION['username'] = $_POST['username'];
				$_SESSION['userType'] = "user";
				header('location:Home.php');
			}else {
			header('location:Index.php');	
		}
		}else {
			header('location:Index.php');	
		}
			
	}


	// ------------------------ Sign up authentication ------------------------------------------------

	else if (isset($_POST['FirstName']) && isset($_POST['LastName']) && isset($_POST['email']) && isset($_POST['UserName']) && isset($_POST['Password1']) && isset($_POST['Password2'])){
		
		//$_SESSION['connect'] ="active";
		
		
		// 1st condition : check username if exist already in your FILE
		$UsernameStatus = "coco";
		$UsernameStatus1 = "coco";
		$UsernameStatus = checkUsernameSignUp("Protected/AccountRequested.txt",$_POST['UserName']); // the value returned is " ": username don't exist | create account or "error": Username exist | ask another username
		$UsernameStatus1 = checkUsernameSignUp("Protected/AccountAccepted.txt",$_POST['UserName']);
		$pwdSecure = "NULL";
		echo "Username : ".$_POST['UserName'];
		
		echo " stat 0 : ".$UsernameStatus;
		echo " ; stat 1 : ".$UsernameStatus1;

		if($UsernameStatus != "coco" || $UsernameStatus1 != "coco")	
			$pwdSecure = "'".$UsernameStatus."' exist already! Change your username please!";
		
		// 2nd condition : check if password is secure
		$pwdSecure1 = checkPasswordSecure($_POST['Password1']); //return blank:ok or error: not ok
		$pwdSecure = $pwdSecure.$pwdSecure1;
		
		// 3rd condition : check if password1 = password2
		$pwdEqual = "NULL";
		$pwdEqual = checkPasswordSignUp($_POST['Password1'], $_POST['Password2']); //return 1: ok or 2: not ok

		$pwdSecure = $pwdSecure.$pwdEqual;
		
		// If everything is allright we create the account in the file
		if ($pwdSecure == "NULL"){ //everything ok we create the account
			$account = "";
			$username = $_POST['UserName'];

			$password = hashPwd($_POST['Password1'], $username);
			$email = $_POST['email'];
			$LastName = $_POST['LastName'];
			$FirstName = $_POST['FirstName'];

			$account = $username." ".$password." ".$email." ".$LastName." ".$FirstName.PHP_EOL;
			$fileName = "Protected/AccountRequested.txt";
			// write the account in the file
			$content = file_get_contents($fileName);
			$content = $content.$account;
			echo $content;
			file_put_contents($fileName, $content);
		}else{
			echo " no";
		}

		header('location:sign_up.php?error='.$pwdSecure);	
	}
	else{
		header('location:Index.php');
	}
?>