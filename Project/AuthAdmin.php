<?php 
	
	// ------------------------ functions start here --------------------------------------------------

	include ('Function.php');

	// ------------------------ functions stops here --------------------------------------------------
	
	// ------------------------ Login authentication-----------------------------------
	
	if(isset($_POST['username']) && isset($_POST['password'])){
		
		//check if password & username exist and match
		$user = $_POST['username'];
		$passwordLogin = $_POST['password'];

		// check recaptcha
		
		$secretKey = "6LcdSIsUAAAAAKGPLk21On6RCVhykPMkRot4qP07";
		$responseKey = $_POST["g-recaptcha-response"];
		$userIP = $_SERVER['REMOTE_ADDR'];

		$url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
		$response = file_get_contents($url);
		$response = json_decode($response);
		if($response->success){
			echo "Captcha ok";
			$username = checkUsernameLogin("Protected/AccountAdmin.txt",$user); // return a string or ""
			$checkLogin = allowUserConnect("Protected/AccountAdmin.txt",$passwordLogin,$username);
			if ($checkLogin == 1){
				session_start();
				$_SESSION['id'] = 1;
				$_SESSION['connect'] ="active";

				$_SESSION['username'] = $_POST['username'];
				$_SESSION['userType'] = "admin";
				header('location:HomeAdmin.php');
			}else{
				header('location:LoginAdmin.php');		
			}
		}else {
			header('location:LoginAdmin.php');	
		}
			
	}


	// ------------------------ Sign up authentication ------------------------------------------------

	else if (isset($_POST['FirstName']) && isset($_POST['LastName']) && isset($_POST['email']) && isset($_POST['UserName']) && isset($_POST['Password1']) && isset($_POST['Password2'])){
		
		//$_SESSION['connect'] ="active";
		
		
		// 1st condition : check username if exist already in your FILE
		$UsernameStatus = " ";
		$UsernameStatus = checkUsernameSignUp("/Protected/AccountAdmin.txt",$_POST['UserName']); // the value returned is " ": username don't exist | create account or "error": Username exist | ask another username
		$pwdSecure =" ";

		if($UsernameStatus[0] != " ")	
			$pwdSecure = "'".$UsernameStatus[0]."' exist already! Change your username please!";
		
		// 2nd condition : check if password is secure
		$pwdSecure1 = checkPasswordSecure($_POST['Password1']); //return blank:ok or error: not ok
		$pwdSecure = $pwdSecure.$pwdSecure1;
		// 3rd condition : check if password1 = password2
		$pwdEqual = "";
		$pwdEqual = checkPasswordSignUp($_POST['Password1'], $_POST['Password2']); //return 1: ok or 2: not ok

		$pwdSecure = $pwdSecure.$pwdEqual;
		
		// If everything is allright we create the account in the file
		if ($pwdSecure == " "){ //everything ok we create the account
			$account = "";
			$username = $_POST['UserName'];
			// we check if the account is not there yet
			$filename ="Protected/AccountAdmin.txt";
			if(!find_value($filename, $username)){
				echo find_value($filename, $username);
				$password = hashPwd($_POST['Password1'], $username);
				$email = $_POST['email'];
				$LastName = $_POST['LastName'];
				$FirstName = $_POST['FirstName'];

				$account = $username." ".$password." ".$email." ".$LastName." ".$FirstName.PHP_EOL;
				$fileName = "Protected/AccountAdmin.txt";
				// write the account in the file
				$content = file_get_contents($fileName);
				$content = $content.$account;
				//echo $content;
				file_put_contents($fileName, $content);
				createFolderAccount($username);
			}else{
				$pwdSecure = $pwdSecure." Account exist already!";
				echo $pwdSecure;
			}
		}
		header('location:sign_upAdmin.php?error='.$pwdSecure);	
	}
	else{
		header('location:LoginAdmin.php');
	}
?>