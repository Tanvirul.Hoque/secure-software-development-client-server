<?php 
	session_start();
		if (isset($_SESSION['userType'])) {
				if($_SESSION['userType'] == "admin")
						header('location:HomeAdmin.php');
			} 
 	include('Function.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Home</title>
</head>
<body>
<?php 
	if ($_SESSION['connect'] != "active"){
		echo "Not even connected";
		session_destroy();
		header("location:Index.php");
	}elseif ($_SESSION['connect'] == "active" ) {
		
		// ------------- Successfully logged in ----------------

		$username = $_SESSION['username'];
		$userType = $_SESSION['userType'];
		$path ="";
		

		if(isset($_POST['path'])){
			$path = $_POST['path'];
		}

		echo "<p>USERNAME : ". $_SESSION['username']."</p>";
		echo "<p><a href='Log_out.php'>Log out</a> | <a href='ChangePassword.php'>Change Password</a></p>";
		

		// AFTER FileProcess.php it goes back here
		
		if(isset($_POST['SaveModify']) && isset($_POST['textArea'])){	// AFTER MODIFY FILE
			$content = $_POST['textArea'];
			$filename = $_POST['SaveModify'];
			file_put_contents($filename, $content);
		}elseif (isset($_POST['SaveRenameFile']) && isset($_POST['textArea'])) { 	// AFTER RENAME FILE
			$NewFileName = "Home/".$username."/".$_POST['textArea'];
			if(file_exists($_POST['SaveRenameFile'])){
				rename($_POST['SaveRenameFile'], $NewFileName);	
			}
		}elseif (isset($_POST['SaveRenameDir']) && isset($_POST['textArea'])) {	// AFTER RENAME DIR
			$NewFileName = "Home/".$username."/".$_POST['textArea'];
			if(is_dir($_POST['SaveRenameDir'])){
				rename($_POST['SaveRenameDir'], $NewFileName);	
			}
		}elseif(isset($_POST['textArea']) && isset($_POST['SaveCreateDir'])){	// AFTER CREATE DIR
			$newDir = $_POST['SaveCreateDir'].$_POST['textArea'];
			if(is_dir($_POST['textArea'])){
			}else{
				createFolder($_POST['textArea'],$_SESSION['username']);
			}
		}elseif(isset($_POST['textArea']) && isset($_POST['SaveCreateFile'])){	// AFTER CREATE FILE
			$newFile = $_POST['SaveCreateFile'].$_POST['textArea'];
			if(!file_exists($_POST['textArea'])){
				createFile($_POST['textArea'],$_SESSION['username']);
			}
		}
	
		// upload | create a file | create a folder
		echo '
			<form method ="post" action="FileProcess.php" enctype="multipart/form-data">
		<table>
		';
		echo '
			<tr>
				<td><input type="file" name ="upload"></td>
				<td><button type="submit" value="Upload" name ="uploadSubmit">Upload</button> | | | </td>
				<td><button type="submit" value="CreateDir" name ="CreateDir">Create DIR</button> | | |  </td>
				<td><button type="submit" value="CreateFile" name ="CreateFile">Create FILE .txt</button></td>
			</tr>
		';


		echo '</table>
			</form>';


		$dir = "Home/".$username.$path;

		echo "<h3>My File : ".$dir."</h3>";


		echo '
			<form method ="post" action="FileProcess.php" enctype="multipart/form-data">
		<table>
		';	
				// LIST ALL FILE IN USER DIRECTORY
		$i = 0;
		if ($handle = opendir($dir)){

		    while (false !== ($entry = readdir($handle))) {

		        if ($entry != "." && $entry != "..") {

		        	$_SESSION['file '.$i] = $entry; 
		        	if(checkDir($entry)){	// DIR 
		        		echo '
					<tr>
						<td><b>'.$entry.'</b></td>
						<td><button type="submit" action ="FileProcess.php" name = "Enter" value="'.$i.'">Enter</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "renameDir" value="'.$i.'">Rename</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DownloadDir" value="'.$i.'">Download</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DeleteDir" value="'.$i.'">Delete</button></td>
					</tr>
		';
		        	}else{	// FILE
		        		echo '
				
					<tr>
						<td>'.$entry.'</td>
						<td><button type="submit" action ="FileProcess.php" name = "modify" value="'.$i.'">Modify</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "renameFile" value="'.$i.'">Rename</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DownloadFile" value="'.$i.'">Download</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DeleteFile" value="'.$i.'">Delete</button></td>

					</tr>
		';
		        	}
		            
		        }
		        $i++;
		    }

		    closedir($handle);
		}
		echo '</table>
			</form>';
	}
	else{	// Else go back to index
		session_destroy();
		header("location:Index.php");
	}
?>
	
</body>
</html>