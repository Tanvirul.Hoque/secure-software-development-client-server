<?php  
	session_start();
	include('Function.php');
	if($_SESSION['username'] == NULL){
		header('location.Index.php');
	}
	// USER

	if(isset($_POST['modify'])){ // MODIFY FILE CONTENT
		$number = $_POST['modify'];
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/".$_SESSION['file '.$number];
		

		//read the file first and then reput all the content
		echo "<p>Function MODIFY FILE :</p>";
		echo "<p><u>File name</u> : ".$_SESSION['file '.$number]."</p>";
		$content = file_get_contents($filename);
		if($_SESSION['userType'] == "user"){
		echo '
			<form method="post" action="Home.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea">'.$content.'</textarea>
				<button type="submit" name ="SaveModify" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}elseif($_SESSION['userType'] === "admin"){
			echo '
			<form method="post" action="HomeAdmin.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea">'.$content.'</textarea>
				<button type="submit" name ="SaveModify" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}
	}elseif(isset($_FILES['upload']) && isset($_POST['uploadSubmit'])){	// UPLOAD FILE
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/";
		$target_dir = $filename;
		if (file_exists($_FILES["upload"]['name'])) {
			echo "exist";

			if($_SESSION['userType'] == "user"){
				header('location:Home.php');
			}elseif($_SESSION['userType'] === "admin"){
				header('location:HomeAdmin.php');
			}
		}else{
			if ($_FILES["upload"]["size"] > 500000) {
				echo "doesn't exist, size not ok";
				if($_SESSION['userType'] == "user"){
				header('location:Home.php');
			}elseif($_SESSION['userType'] === "admin"){
				header('location:HomeAdmin.php');
			}
			}else{
				echo " size ok";
				$target_file = $target_dir . basename($_FILES["upload"]["name"]);	
				if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_file)) {
			        echo "The file ". basename( $_FILES["upload"]["name"]). " has been uploaded.";
			        if($_SESSION['userType'] == "user"){
						header('location:Home.php');
					}elseif($_SESSION['userType'] === "admin"){
						header('location:HomeAdmin.php');
					}
			    } else {
			        echo "Sorry, there was an error uploading your file.";
			        if($_SESSION['userType'] == "user"){
						header('location:Home.php');
					}elseif($_SESSION['userType'] === "admin"){
						header('location:HomeAdmin.php');
					}
			    }
			}
			if($_SESSION['userType'] == "user"){
				header('location:Home.php');
			}elseif($_SESSION['userType'] === "admin"){
				header('location:HomeAdmin.php');
			}
		}
	}elseif(isset($_POST['CreateDir'])){	// CREATE DIR
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/";

		echo "<p>Function CREATE DIR:</p>";		
		if($_SESSION['userType'] == "user"){
		echo '
			<form method="post" action="Home.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea"></textarea>
				<button type="submit" name ="SaveCreateDir" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}elseif($_SESSION['userType'] === "admin"){
			echo '
			<form method="post" action="HomeAdmin.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea"></textarea>
				<button type="submit" name ="SaveCreateDir" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}

	}elseif(isset($_POST['CreateFile'])){ // CREATE FILE
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/";

		echo "<p>Function CREATE FILE (.txt):</p>";		
		
		echo '
			<form method="post" action="Home.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea"></textarea>
				<button type="submit" name ="SaveCreateFile" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';		

	}elseif (isset($_POST['renameFile'])){	// RENAME FILE 
		$number = $_POST['renameFile'];
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/".$_SESSION['file '.$number];
		

		//read the file first and then reput all the content
		echo "<p>Function RENAME FILE:</p>";
		echo "<p><u>Actual name</u> : ".$_SESSION['file '.$number]."</p>";
		if($_SESSION['userType'] == "user"){
				echo '
			<form method="post" action="Home.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea">'.$_SESSION['file '.$number].'</textarea>
				<button type="submit" name ="SaveRenameFile" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}elseif($_SESSION['userType'] === "admin"){
				echo '
			<form method="post" action="HomeAdmin.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea">'.$_SESSION['file '.$number].'</textarea>
				<button type="submit" name ="SaveRenameFile" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}
		
	}elseif (isset($_POST['renameDir'])){	// RENAME DIR 
		$number = $_POST['renameDir'];
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/".$_SESSION['file '.$number];
		

		//read the file first and then reput all the content
		echo "<p>Function RENAME DIR:</p>";
		echo "<p><u>Actual DIR name</u> : ".$_SESSION['file '.$number]."</p>";
		
		if($_SESSION['userType'] == "user"){
				echo '
			<form method="post" action="Home.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea">'.$_SESSION['file '.$number].'</textarea>
				<button type="submit" name ="SaveRenameDir" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}elseif($_SESSION['userType'] === "admin"){
				echo '
			<form method="post" action="HomeAdmin.php" enctype="multipart/form-data">
				<textarea type="text" name="textArea">'.$_SESSION['file '.$number].'</textarea>
				<button type="submit" name ="SaveRenameDir" value="'.$filename.'">Save</button>
				<button type="submit" name ="Cancel">Cancel</button>
			</form>
		';
			}
		
	}elseif(isset($_POST['DownloadFile'])){	// DOWNLOAD FILE
		$number = $_POST['DownloadFile'];
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/".$_SESSION['file '.$number];

		$file_url = $filename;
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename=" . basename($file_url)); 
		readfile($file_url);
		
	}elseif (isset($_POST['DownloadDir'])) {	// DOWNLOAD DIR WITH ZIP
		$number = $_POST['DownloadDir'];
		$username = $_SESSION['username'];
		$dir = "Home/".$username."/".$_SESSION['file '.$number];


		$zip_file = 'Download.zip';

		// Get real path for our folder
		$rootPath = realpath($dir);

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
		    new RecursiveDirectoryIterator($rootPath),
		    RecursiveIteratorIterator::LEAVES_ONLY
		);

		foreach ($files as $name => $file)
		{
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir())
		    {
		        // Get real and relative path for current file
		        $filePath = $file->getRealPath();
		        $relativePath = substr($filePath, strlen($rootPath) + 1);

		        // Add current file to archive
		        $zip->addFile($filePath, $relativePath);
		    }
		}

		// Zip archive will be created only after closing object
		$zip->close();


		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($zip_file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($zip_file));
		readfile($zip_file);
	}elseif(isset($_POST['DeleteDir'])){	// DELETE DIR
		$number = $_POST['DeleteDir'];
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/".$_SESSION['file '.$number];
		echo $username;
		rrmdir($filename);
		
		if($_SESSION['userType'] == "user"){
			header('location:Home.php');
		}elseif($_SESSION['userType'] === "admin"){
			header('location:HomeAdmin.php');
		}
		

	}elseif(isset($_POST['DeleteFile'])){	// DELETE FILE
		$number = $_POST['DeleteFile'];
		$username = $_SESSION['username'];
		$filename = "Home/".$username."/".$_SESSION['file '.$number];
		deleteFile($_SESSION['file '.$number], $username);
		if($_SESSION['userType'] == "user"){
				header('location:Home.php');
			}elseif($_SESSION['userType'] === "admin"){
				header('location:HomeAdmin.php');
			}
	}else{
		echo "other";
			if(isset($_SESSION['userType'])){
				//if($_SESSION['userType'] == "user")
						//header('location:Home.php');
			}elseif (isset($_SESSION['userType'])) {
				//if($_SESSION['userType'] == "admin")
						//header('location:HomeAdmin.php');
			}else{
				header('location:Index.php');
			}
		header('location:Home.php');
		}

	// ADMIN

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	

</body>
</html>