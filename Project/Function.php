<?php 
	//-------------------------Function for home.php-------------------------------------- 
		//||||||| create a file
		function createFile($fileName, $username){

			$fileName = "Home/".$username."/".$fileName;
			$myfile = fopen($fileName, "w");
			fclose($myfile);
		}
		//||||||| write in a file
		function writeFile($fileName,$value){
			$myfile = fopen($fileName, "w") or die("Unable to open file!");
			fwrite($myfile, $value);
			fclose($myfile);
		}
		//||||||| Delete a file
		function deleteFile($fileName, $username){
			$myFile = "Home/".$username."/".$fileName;
			unlink($myFile) or die("Couldn't delete file");
		}

		// ||||||||  Delete a folder and all it's content
		function deleteFolder($dir, $username) {
			$target = "Home/".$username."/".$dir;
		    if(is_dir($target)){
		        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

		        foreach( $files as $file ){
		            deleteFolder( $file, $username );      
		        }

		        rmdir( $target );
		    } elseif(is_file($target)) {
		        unlink( $target );  
		    }
		}

		// ||||||||  Delete a folder and all it's content LAST CHANCE
		function deleteFolderA($dir) {
			$target = "Home/".$dir;
		    if(is_dir($target)){
		        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

		        foreach( $files as $file ){
		            deleteFolderA( $file );      
		        }

		        rmdir( $target );
		    } elseif(is_file($target)) {
		        unlink( $target );  
		    }
		}

			// RMDIR FUNCTION
		function rrmdir($dir) { 
		   if (is_dir($dir)) { 
		     $objects = scandir($dir); 
		     foreach ($objects as $object) { 
		       if ($object != "." && $object != "..") { 
		         if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
		       } 
		     } 
		     reset($objects); 
		     rmdir($dir); 
		   } 
		 } 
		// ||||||||  Delete an account folder and all it's content
		function deleteFolderAccount($username) {
			$target = "Home/".$username."/";
		    if(is_dir($target)){
		        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

		        foreach( $files as $file ){
		            deleteFolderAccount( $file );      
		        }

		        rmdir( $target );
		    } elseif(is_file($target)) {
		        unlink( $target );  
		    }
		}
		//||||||| Read a file | return the value of the file
		function readMyFile($fileName){
			$myfile = fopen($fileName, "r") or die("Unable to open file!");
			$value = fread($myfile,filesize($fileName));
			fclose($myfile);
			return $value;
		}
		//||||||| Upload file
		function uploadFile($path){
			if(!empty($_FILES['uploaded_file'])){
			    $path = $path . basename( $_FILES['uploaded_file']['name']);
			    
			    if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
			      echo "The file ".  basename( $_FILES['uploaded_file']['name']). 
			      " has been uploaded";
			      header( "refresh:2;url=Home.php" );
			    } else{
			        echo "There was an error uploading the file, please try again!";
			    }
			}
		}

		//||||||| Download any document from server function
		function downloadFile($fileName){
			echo "<a href='".$fileName."'>Download this</a>";
		}

		//||||||| Create a folder or directory for user with mode (rules) 
		function createFolder($folderName, $username){
			$a = $folderName;
			$folderName = "Home/".$username."/".$folderName;
			if (!is_dir($folderName)) {
    			mkdir($folderName);         
				echo "'".$a."' folder is created!";
			}
			else{
				echo "Folder exist already!";
			}
		}

		//||||||| Create a folder or directory for user with mode (rules) 
		function createFolderUP($username){
			$a = $folderName;
			$folderName = "Home/".$username;
			if (!is_dir($folderName)) {
    			mkdir($folderName);         
				echo "'".$a."' folder is created!";
			}
			else{
				echo "Folder exist already!";
			}
		}

			// find a  word in a file
		function find_value($fileName, $input){
		// $input is the word being supplied by the user
		$handle = @fopen($fileName, "r");
		if ($handle){
		  while (!feof($handle)){
		    $entry_array = explode(" ",fgets($handle));
		    if ($entry_array[0] == $input){
		      return 1;
		      }
		    }
		  fclose($handle);
		  }
		return 0;
		}

		//||||||| Create a folder or directory When the account is created
		function createFolderAccount($username){
			$a = $username;
			$folderName = "Home/".$username."/";
			if (!is_dir($folderName)) {
    			mkdir($folderName);         
				echo "'".$a."' folder is created!";
			}
			else{
				echo "Folder exist already!";
			}
		}

		//||||||| Delete a folder
		function deleteFolderFirst($folderName){
		    if(is_dir($folderName)){
		        $files = glob($folderName . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

		        foreach($files as $file){
		            deleteFolder($file);      
		        }
		        if(is_dir($folderName))
		        	rmdir($folderName);
		    echo "'".$folderName ."' folder is deleted!<br>";
		    } elseif(is_file($folderName)) {
		        unlink($folderName);  
		    }
		}

		// CHECK IF A SELECTED ONE IS A DIR OR  FILE

				function checkDir($name){
			for($i = 0; $i < strlen($name); $i++){
				if($name[$i] == "."){
					return 0;
				}
			}
			return 1;
		}

	//-------------------------Functions for login.php--------------------------------- 
	
		////////// check username if exist in the file
	function checkUsernameLogin($file,$usernameToCheck){
		$handle = fopen($file, "r");
		$value = "";
		if ($handle) {
		    while (($line = fgets($handle)) !== false) {
		        // process the line read.
		        $word = explode(" ", $line);
		        if($usernameToCheck == $word[0]){
		        	$value = $line;
		        	return $value;
		        }
	    	}
	    	fclose($handle);
		}
		return $value; //if value return "" (blank) the username is wrong - if value return something username exist
	}

		///////////// password dehash to compare with what the user input
	function passwordDehash($username, $password, $passwordHashed){
		$hashedU = hash('sha256', $username);
		$check = $password.$hashedU;
		if (password_verify($check, $passwordHashed)) {
	    	return 1; // return 1 if it is valid
		} else {
		    return 0; // return 0 if it is not valid
		}
	}

		////////// last function before letting the user log in
	function allowUserConnect($fileName, $password, $account){
		if($account != ""){
			$Account = explode(" ", $account);
			$dehash = passwordDehash($Account[0], $password, $Account[1]); // return 1 if ok and 2 if not ok
		}
		else
			$dehash = 2;
		return $dehash; // 1 if ok and 2 if not ok
	}
	
	//-------------------------Functions for Sign up.php-------------------------------------- 
	

	// ||||||| function to check if the username exist already
	function checkUsernameSignUp($file,$usernameToCheck){
		$handle = @fopen($file, "r");
		$i = 0;
		$ok = "coco";

		// check the username
		if ($handle) {
			
		    while (!feof($handle)) {
		        $buffer = fgets($handle); // fgets: Function to get a line in a file
		        $exploded_data = explode(" ",$buffer); // explode take word per word and put it into an array which is $exploded_data
		        if ($usernameToCheck == $exploded_data[0]){ //check if the username exist already
		        	$ok = $exploded_data[0];
		        	echo "Username already exist, please choose another username!<br>";
		 			return $ok;
		        }
		    	$i++; // number of account = $i-1

		    }
		    fclose($handle);
		}else{
			echo "<p>Sorry didn't read the file</p>";
		}

		return $ok; // if ok equal "coco" => username does not exist
	}



	// ||||||| function: check if password is secure enough
	function checkPasswordSecure($pwd) {
    $errors = "";

    if (strlen($pwd) < 8) {
        $errors = $errors . "Password too short!<br>";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors= $errors . "Password must include at least one number!<br>";
    }

    if (!preg_match("#[a-zA-Z]+#", $pwd)) {
        $errors= $errors . "Password must include at least one letter!<br>";
    }     

    	return $errors;
	}	


	// ||||||| function: check if the password are equal
	function checkPasswordSignUp($pwd1, $pwd2){
		if($pwd1 == $pwd2){
			return "";
		}
		else{
			return "Your password is not equal retype again!";
		}
	}

	// hashing the password before saving into the file
	function hashPwd($password, $username){
		$salt = hash("sha256", $username);
		$passwordToHash = $password.$salt;
		$value = password_hash($passwordToHash, PASSWORD_BCRYPT);	
		return $value;
	}
?>