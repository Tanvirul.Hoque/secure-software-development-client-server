<?php  
	session_start();
	if(isset($_SESSION['userType'])){
		if($_SESSION['userType'] == "user")
				header('location:Home.php');
	}elseif (isset($_SESSION['userType'])) {
		if($_SESSION['userType'] == "admin")
				header('location:HomeAdmin.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Admin</title>
	
</head>
<body>
	
<form method="post" action="AuthAdmin.php">
	<table>
		<tr>
			<h2><b>ADMIN</b> Log in 10<font color="red">X</font></h2>
		</tr>
		<tr>
			<td>Username</td>
			<td>Password</td>
		</tr>
		<tr>
			<td><input type="text" name="username" placeholder="Enter your username" required></td>
			<td><input type="password" name="password" placeholder="Enter your password" required></td>
			<td><div class="g-recaptcha" data-sitekey="6LcdSIsUAAAAAGQChYE5Kwb45m3iOLJ8iJkInDfy"></div></td>
			<td><input type="submit" name="connect" value="Log in"></td>
		</tr>
	</table>
</form>
	
<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>