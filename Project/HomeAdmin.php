<?php 
	session_start(); 
		if(isset($_SESSION['userType'])){
				if($_SESSION['userType'] == "user")
						header('location:Home.php');
			}
 	include('Function.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Home Admin</title>
</head>
<body>
<?php 
	if ($_SESSION['connect'] != "active"){
		echo "Not even connected";
		session_destroy();
		header("location:LoginAdmin.php");
	}elseif ($_SESSION['connect'] == "active" ) {
		
		// ------------- Successfully logged in ----------------

		$username = $_SESSION['username'];
		$userType = $_SESSION['userType'];
		$path ="";
		

		if(isset($_POST['path'])){
			$path = $_POST['path'];
		}

		echo "<p>USERNAME : ". $_SESSION['username']."</p>";
		echo "<p><a href='Log_outAdmin.php'>Log out</a> | <a href='ChangePassword.php'>Change Password</a> | <a href='HomeAdmin.php'>Refresh the page</a></p>";
		

		// AFTER FileProcess.php it goes back here
		
		if(isset($_POST['SaveModify']) && isset($_POST['textArea'])){	// AFTER MODIFY FILE
			$content = $_POST['textArea'];
			$filename = $_POST['SaveModify'];
			file_put_contents($filename, $content);
		}elseif (isset($_POST['SaveRenameFile']) && isset($_POST['textArea'])) { 	// AFTER RENAME FILE
			$NewFileName = "Home/".$username."/".$_POST['textArea'];
			if(file_exists($_POST['SaveRenameFile'])){
				rename($_POST['SaveRenameFile'], $NewFileName);	
			}
		}elseif (isset($_POST['SaveRenameDir']) && isset($_POST['textArea'])) {	// AFTER RENAME DIR
			$NewFileName = "Home/".$username."/".$_POST['textArea'];
			if(is_dir($_POST['SaveRenameDir'])){
				rename($_POST['SaveRenameDir'], $NewFileName);	
			}
		}elseif(isset($_POST['textArea']) && isset($_POST['SaveCreateDir'])){	// AFTER CREATE DIR
			$newDir = $_POST['SaveCreateDir'].$_POST['textArea'];
			if(is_dir($_POST['textArea'])){
			}else{
				createFolder($_POST['textArea'],$_SESSION['username']);
			}
		}elseif(isset($_POST['textArea']) && isset($_POST['SaveCreateFile'])){	// AFTER CREATE FILE
			$newFile = $_POST['SaveCreateFile'].$_POST['textArea'];
			if(!file_exists($_POST['textArea'])){
				createFile($_POST['textArea'],$_SESSION['username']);
			}
		}
	
		// upload | create a file | create a folder
		echo '
			<form method ="post" action="FileProcess.php" enctype="multipart/form-data">
		<table>
		';
		echo '
			<tr>
				<td><input type="file" name ="upload"></td>
				<td><button type="submit" value="Upload" name ="uploadSubmit">Upload</button> | | | </td>
				<td><button type="submit" value="CreateDir" name ="CreateDir">Create DIR</button> | | |  </td>
				<td><button type="submit" value="CreateFile" name ="CreateFile">Create FILE .txt</button></td>
			</tr>
		';


		echo '</table>
			</form>';


		$dir = "Home/".$username.$path;

		echo "<h3>My File : ".$dir."</h3>";


		echo '
			<form method ="post" action="FileProcess.php" enctype="multipart/form-data">
		<table>
		';	
				// LIST ALL FILE IN ACTUAL ACCOUNT DIRECTORY
		$i = 0;
		if ($handle = opendir($dir)){

		    while (false !== ($entry = readdir($handle))) {

		        if ($entry != "." && $entry != "..") {

		        	$_SESSION['file '.$i] = $entry; 
		        	if(checkDir($entry)){	// DIR 
		        		echo '
					<tr>
						<td><b>'.$entry.'</b></td>
						<td><button type="submit" action ="FileProcess.php" name = "Enter" value="'.$i.'">Enter</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "renameDir" value="'.$i.'">Rename</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DownloadDir" value="'.$i.'">Download</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DeleteDir" value="'.$i.'">Delete</button></td>
					</tr>
		';
		        	}else{	// FILE
		        		echo '
				
					<tr>
						<td>'.$entry.'</td>
						<td><button type="submit" action ="FileProcess.php" name = "modify" value="'.$i.'">Modify</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "renameFile" value="'.$i.'">Rename</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DownloadFile" value="'.$i.'">Download</button></td>
						<td><button type="submit" action ="FileProcess.php" name = "DeleteFile" value="'.$i.'">Delete</button></td>

					</tr>
		';
		        	}
		            
		        }
		        $i++;
		    }

		    closedir($handle);
		}
		echo '</table>
			</form>';

			// ACCOUNT MANAGEMENT
	echo "<h3>Manage account requested : </h3>";

	echo '
			<form method ="post" action="UserManagement.php" enctype="multipart/form-data">
		<table>
		';
		$file = "Protected/AccountRequested.txt";
		$handle = @fopen($file, "r");
		$s = 0;
		$ok = " ";
		// check the username
		if ($handle) {
		    while (!feof($handle)) {
		        $buffer = fgets($handle); // fgets: Function to get a line in a file
		        $exploded_data = explode(" ",$buffer); // explode take word per word and put it into an array which is $exploded_data
		        $_SESSION['account'.$s] = $exploded_data[0];
		        if($exploded_data[0] != ""){
		        	echo '
					
						<tr>
							<td>'.$exploded_data[0].'</td>
				
						<td><button type="submit" action ="UserManagement.php" name = "Accept" value="'.$s.'">Accept</button></td>
						<td><button type="submit" action ="UserManagement.php" name = "Refuse" value="'.$s.'">Refuse</button></td>

					</tr>
					';
		        }
			
				
		    	$s++; // number of account = $i-1
		    }
		    fclose($handle);
		}

	echo '</table>
			</form>';
	
	// MANAGE ACCOUNT ACCEPTED
	echo "<h3>Manage account accepted : </h3>";

	echo '
			<form method ="post" action="UserManagement.php" enctype="multipart/form-data">
		<table>
		';
		$file = "Protected/AccountAccepted.txt";
		$handle1 = @fopen($file, "r");
		$b = 0;
		// check the username
		if ($handle1) {
		    while (!feof($handle1)) {
		        $buffer = fgets($handle1); // fgets: Function to get a line in a file
		        $exploded_data = explode(" ",$buffer); // explode take word per word and put it into an array which is $exploded_data		        
		        	if($exploded_data[0] != " " && $exploded_data[0] != "" && $exploded_data[0] != NULL){
		        		$_SESSION['accountAccepted'.$b] = $exploded_data[0];
			        	echo '
							<tr>
								<td>'.$_SESSION['accountAccepted'.$b].'</td>
							<td><button type="submit" action ="UserManagement.php"  name = "Delete" value="'.$b.'">Delete</button></td>
							<td><button type="submit" action ="UserManagement.php" name = "ChangePassword" value="'.$b.'">Change password</button></td>

						</tr>
						';	
		        	}
		        	
		    	$b++; // number of account = $i-1
		    }
		    fclose($handle1);
		}

	echo '</table>
			</form>';
	



	}else{	// Else go back to index
		session_destroy();
		header("location:LoginAdmin.php");
	}
?>
	
</body>
</html>